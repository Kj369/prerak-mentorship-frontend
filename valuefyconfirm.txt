{
    "confirmTransactionForBocReq": {
        "msgHdr": {
            "frm": {
                "id": "BOT"
            },
            "hdrFlds": {
                "msgId": "12345",
                "timestamp": "2022-12-26T07:22:40"
            }
        },
        "msgBdy": {
            "header": {
                "source": "external-system",
                "version": "v1.0",
                "api-key": "AdvD2QgZXhhbXBsZSBmbyBob"
            },
            "referenceNumber": "RMORD00011911",
            "updateFlag": "1",
            "intimationFlag": "M"
        }
    },
    "userLogData": {
        "appId": "031",
        "stepId": "8",
        "uniqueId": "5304636023|1000474382"
    }
}


{
    "errorMessage": "Request processing failed; nested exception is com.idfcfirstbank.services.exception.RestTemplateException: {  \"confirmTransactionForBocResp\": {    \"msgHdr\": {      \"rslt\": \"ERROR\",      \"error\": [        {          \"cd\": \"SYS_504\",          \"rsn\": \"Request Timeout.\",          \"srvc\": {            \"nm\": \"valuefy-wealth-sys\",            \"cntxt\": \"valuefy-wealth-sys\",            \"actn\": {              \"paradigm\": \"Reply\",              \"nm\": \"confirmValuefyTransactionForBoc\",              \"vrsn\": \"1\"            }          }        }      ]    },    \"msgBdy\": {          }  }}"
}